from telethon.errors import ChannelInvalidError, ChannelPrivateError, ChannelPublicGroupNaError
from telethon.tl.types import MessageActionChannelMigrateFrom, ChannelParticipantsAdmins, ChatAdminRights, MessageEntityMentionName
from telethon.tl.functions.messages import GetHistoryRequest, GetFullChatRequest
from telethon.tl.functions.channels import GetFullChannelRequest, GetParticipantsRequest, EditAdminRequest
from telethon.tl.functions.phone import GetGroupCallRequest
from userbot import bot 


async def get_user_from_event(event):
    args = event.pattern_match.group(1).split(' ', 1)
    extra = None
    if event.reply_to_msg_id and not len(args) == 2:
        previous_message = await event.get_reply_message()
        user_obj = await event.client.get_entity(previous_message.from_id)
        extra = event.pattern_match.group(1)
    elif args:
        user = args[0]
        if len(args) == 2:
            extra = args[1]

        if user.isnumeric():
            user = int(user)

        if not user:
            await event.edit("`İstifadəçi adı, ID və ya mesajını yönləndirin!`")
            return

        if event.message.entities is not None:
            probable_user_mention_entity = event.message.entities[0]

            if isinstance(probable_user_mention_entity,
                          MessageEntityMentionName):
                user_id = probable_user_mention_entity.user_id
                user_obj = await event.client.get_entity(user_id)
                return user_obj, extra
        try:
            user_obj = await event.client.get_entity(user)
        except Exception as err:
            await event.edit(str(err))
            return None

    return user_obj, extra


async def get_user_from_id(user, event):
    if isinstance(user, str):
        user = int(user)

    try:
        user_obj = await event.client.get_entity(user)
    except Exception as err:
        await event.edit(str(err))
        return None

    return user_obj


async def get_full_user(event):
    user = event.pattern_match.group(1).split(":", 1)
    extra = None
    if event.reply_to_msg_id and not len(user) == 2:
        previous_message = await event.get_reply_message()
        user_obj = await event.client.get_entity(previous_message.sender_id)
        extra = event.pattern_match.group(1)
    elif len(user[0]) > 0:
        user = user[0]
        if len(user) == 2:
            extra = user[1]
        if user.isnumeric():
            user = int(user)
        if not user:
            await event.edit("İstifadəçi adı, ID və ya mesajını yönləndirin!`")
            return
        if event.message.entities is not None:
            probable_user_mention_entity = event.message.entities[0]
            if isinstance(probable_user_mention_entity, MessageEntityMentionName):
                user_id = probable_user_mention_entity.user_id
                user_obj = await event.client.get_entity(user_id)
                return user_obj
        try:
            user_obj = await event.client.get_entity(user)
        except Exception as err:
            return await event.edit(
                "Bir xəta baş verdi:\nBunu @UseratorSUP’a bildirin", str(err)
            )
    return user_obj, extra


async def get_call(event):
    chat = await bot(GetFullChannelRequest(event.chat_id))
    call = await bot(GetGroupCallRequest(chat.full_chat.call))
    return call.call


async def get_chatinfo(event):
    chat = event.pattern_match.group(1)
    chat_info = None
    if chat:
        try:
            chat = int(chat)
        except ValueError:
            pass
    if not chat:
        if event.reply_to_msg_id:
            replied_msg = await event.get_reply_message()
            if replied_msg.fwd_from and replied_msg.fwd_from.channel_id is not None:
                chat = replied_msg.fwd_from.channel_id
        else:
            chat = event.chat_id
    try:
        chat_info = await event.client(GetFullChatRequest(chat))
    except:
        try:
            chat_info = await event.client(GetFullChannelRequest(chat))
        except ChannelInvalidError:
            await event.reply("`Keçərsiz kanal/qrup`")
            return None
        except ChannelPrivateError:
            await event.reply("`Bura bir gizli qrupdur və ya mən buradan ban olmuşam.`")
            return None
        except ChannelPublicGroupNaError:
            await event.reply("`Belə bir superqrup və ya kanal yoxdur`")
            return None
        except (TypeError, ValueError) as err:
            await event.reply(str(err))
            return None
    return chat_info
