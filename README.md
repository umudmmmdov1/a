

<h1 align="center"><b>U S Σ R Δ T O R</b></h1>
<h3 align="center">Userator, Telegram işlətməyinizi asandlaşdıran bir botdur. Tamamilə açıq qaynağlı və ödənişsizdir.</h3>
<h3 align="center">Userator is a bot that makes it easy to use Telegram. Completely open source and free.</h3>
<h1 align="center">DMCA: badge ID b9e12fef-12b8-44ae-9f98-0c963f14a37f</h1>
GPL-3.0 Licence
<center><a href="https://www.dmca.com/Protection/Status.aspx?id=b9e12fef-12b8-44ae-9f98-0c963f14a37f&refurl=https://github.com/sahibziko/usearor" title="DMCA.com Protection Status" class="dmca-badge"> <img src ="https://images.dmca.com/Badges/dmca-badge-w200-5x1-10.png?ID=b9e12fef-12b8-44ae-9f98-0c963f14a37f"  alt="DMCA.com Protection Status" /></a> 
<img src="https://i.hizliresim.com/hkhm4be.PNG" alt="Contributors" /><center/>
<a href="https://github.com/MoveAngel/One4uBot/graphs/contributors"><img src="https://img.shields.io/github/contributors-anon/MoveAngel/One4uBot?label=Contributors&style=flat-square&logo=github&color=FF4D80" alt="Contributors" /></a>
<a href="https://github.com/MoveAngel/One4uBot/watchers"><img src="https://img.shields.io/github/watchers/MoveAngel/One4uBot?label=Watch&style=flat-square&logo=github&color=FF70A6" alt="Watch" /></a>
<a href="https://github.com/MoveAngel/One4uBot/stargazers"><img src="https://img.shields.io/github/stars/MoveAngel/One4uBot?label=Stars&style=flat-square&logo=github&color=F87575" alt="Stars" /></a>
<a href="https://github.com/MoveAngel/One4uBot/network/members"><img src="https://img.shields.io/github/forks/MoveAngel/One4uBot?label=Fork&style=flat-square&logo=github&color=E0777D" alt="Fork" /></a>
<a href="https://hub.docker.com/r/movecrew/one4ubot"> <img src="https://img.shields.io/docker/image-size/movecrew/one4ubot/alpine-latest?label=Docker%20Size&style=flat-square&logo=docker&logoColor=white&color=1B98E0" alt="Docker Image" /></a><br>
<a href="https://t.me/userbotindo"> <img src="https://img.shields.io/badge/telegram-Support_Group-blue?style=social&logo=telegram" alt="Support" /></a>
<a href="https://t.me/userbotindo"> <img src="https://img.shields.io/badge/telegram-Support_Group-blue?style=social&logo=telegram" alt="Channel" /></a>
</p>

## Kömək / Support

<a href="https://t.me/UseratorOT"><img src="https://img.shields.io/badge/Join-Telegram%20Channel-red.svg?logo=Telegram"></a>

<a href="https://t.me/UseratorSup"><img src="https://img.shields.io/badge/Join-Telegram%20Group-blue.svg?logo=telegram"></a>

## Qurulum / Method

<a href="https://youtu.be/fBBJoU1uV-w"><img src="https://static.wikia.nocookie.net/logopedia/images/9/90/YouTube_logo_2005.svg/revision/latest/scale-to-width-down/340?cb=20160807125041"></a>

**Android üçün:** Termuxu açın bu kodu yapışdırın: `bash <(curl -L https://git.io/JkrSb)`

**iOS üçün:** iSH açın bu kodu yapışdırın: `apk update && apk add bash && apk add curl && curl -L -o dto_installer.sh https://git.io/Jkr9n && chmod +x dto_installer.sh && bash dto_installer.sh`


### Heroku ilə deploy / Deploying To Heroku

[![Deploy](https://www.herokucdn.com/deploy/button.svg)](https://heroku.com/deploy?template=https://github.com/umudmmmdov1/dtouserbot)

## Məlumat / Info
<h5 align="center">Botun yanlış istifadəsi halında məsuliyyət tamamilə istifadəçiyə
aiddir.Userator idarəçiləri olaraq heç bir məsuliyyət qəbul etmirik.
Botu telegramı daha rahat istifadə eləmək xaricində əylənmək
üçün də istifadə edə bilərsiniz.</h5>

### Lisenziya / License

<a href="https://tr.m.wikipedia.org/wiki/MIT_Lisans%C4%B1"><img src="https://upload.wikimedia.org/wikipedia/commons/0/0c/MIT_logo.svg"></a>

## Yaradıcı / Creator

[Ümüd Məmmədov](https://t.me/umudmmmdov1)

## Təşəkkürlər / Thanks 

[Hüseyn Abbasov](https://t.me/hus3yns)

[BristolMyers](http://t.me/BristolMyers)
